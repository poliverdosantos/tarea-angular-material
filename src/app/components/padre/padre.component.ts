import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {
obj1!:any;
obj2!:any;

  constructor() { }

  ngOnInit(): void {
  }

  recibirObjeto1($event:any):void{
    this.obj1 = $event;
    console.log(this.obj1);
  }

  recibirObjeto2($event:any):void{
    this.obj2 = $event;
    console.log(this.obj2);
    
  }
}
